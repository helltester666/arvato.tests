﻿namespace Arvato.Tests.Models.Dtos
{
    public class UnauthorizedResponseDto
    {
        public string Message { get; set; }
    }
}

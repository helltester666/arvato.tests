﻿using Arvato.Tests.ApiObjects;
using Arvato.Tests.ApiObjects.BankAccount;
using Arvato.Tests.Steps.Enums;
using BoDi;
using NUnit.Framework;
using RestSharp;
using System;
using System.Net;
using TechTalk.SpecFlow;

namespace Arvato.Tests.Steps.Shared
{
    [Binding]
    public class RequestsSteps
    {
        private readonly IObjectContainer _objectContainer;

        public RequestsSteps(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        }

        /*
         * enum is for creating huge amount of request types in one method
         * Using reflection here would be much more readable and maintainable
         * than have 100+ case statements, but for current task it is enough
         */
        [Given(@"a (.*) request with (valid|invalid) Auth token")]
        public void GivenARequestTypeWithAValidToken(RequestTypes requestType, bool withKey)
        {
            ApiRequestBase request;
            switch (requestType)
            {
                case RequestTypes.ValidateAccount:
                    request = new PostValidateBankAccount();
                    break;
                default: throw new NotImplementedException($"Request type {requestType.ToString()} not implemented.");
            }

            switch (withKey)
            {
                case true:
                    request.WithValidAuthKey();
                    break;
                case false:
                    request.WithInvalidAuthKey();
                    break;
            }

            request.WithValidContentType();
            _objectContainer.RegisterInstanceAs(request);
        }

        [When(@"request is posted to api")]
        public void WhenRequestIsPostedToApi()
        {
            var request = _objectContainer.Resolve<ApiRequestBase>();
            var result = request.Execute();
            _objectContainer.RegisterInstanceAs(result);
        }

        [Then(@"api returns (.*) http code")]
        public void ThenApiReturnsUnauthorizedHttpCode(HttpStatusCode code)
        {
            var response = _objectContainer.Resolve<IRestResponse>();
            Assert.AreEqual(code, response.StatusCode
                , $"Expected {(int)code} status code, but was {(int)response.StatusCode}");
        }

    }
}
﻿namespace Arvato.Tests
{
    public class AppConfig
    {
        public string BaseUrl { get; set; }

        public string AuthKey { get; set; }

        public string CorrectBankAccount { get; set; }
    }
}

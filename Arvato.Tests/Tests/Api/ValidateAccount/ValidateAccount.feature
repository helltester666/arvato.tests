﻿# I know about Background keyword, but i do not use it for only reason:
# 1. There's 1 scenario with given invalid token (and I think
# it should be moved to another feature file that chacks authorization)
Feature: Validating accounts
	In order to verify bank accounts
	As an employee
	I want to check if IBAN format is met

### Positive tests

# I know about tags but don't know which I should use here
Scenario: Valid JWT token and account number should return OK
	Given a ValidateAccount request with valid Auth token
		And ValidateAccount body with valid account number
	When request is posted to api
	Then api returns OK http code
		And ValidateAccount api returns valid verification result

# I need correct numbers to make this test works
Scenario Outline: Сorrect length of account number
	Given a ValidateAccount request with valid Auth token
		And ValidateAccount body with account number "<AccountNumber>"
	When request is posted to api
	Then api returns OK http code
		And ValidateAccount api returns valid verification result

	Examples:
	| AccountNumber                    | 
	| GB09HA0                          |
	| GB09HAOE913118080023171180800231 |

# I need correct numbers to make this test works
# From requirements: api must support only several countries
# I used this doc: https://en.wikipedia.org/wiki/International_Bank_Account_Number
Scenario Outline: Available countries of account number
	Given a ValidateAccount request with valid Auth token
		And ValidateAccount body with account number "<AccountNumber>"
	When request is posted to api
	Then api returns OK http code
		And ValidateAccount api returns valid verification result

	Examples:
	| AccountNumber          |
	| GB09HAOE91311808002317 |
	| AT09HAOE91311808002317 |
	| DE09HAOE91311808002317 |
	| CH09HAOE91311808002317 |
	| FI09HAOE91311808002317 |
	| SE09HAOE91311808002317 |
	| NO09HAOE91311808002317 |
	| DK09HAOE91311808002317 |
	
### Negative tests

Scenario: Invalid JWT token should return Unauthorized with error message
	Given a ValidateAccount request with invalid Auth token
		And ValidateAccount body with valid account number
	When request is posted to api
	Then api returns Unauthorized http code
		And ValidateAccount api returns "Authorization has been denied for this request." message

Scenario Outline: Incorrect length of account number
	Given a ValidateAccount request with valid Auth token
		And ValidateAccount body with account number "<AccountNumber>"
	When request is posted to api
	Then api returns Bad Request http code
		And ValidateAccount api returns "<Error>" in error message

	Examples:
	| AccountNumber                     | Error                                              |
	| GB09HA                            | A string value with minimum length 7 is required.  |
	| GB09HAOE9131180800231711808002317 | A string value with maximum length 32 is required. |

Scenario: Correct format of number with failed validation
	Given a ValidateAccount request with valid Auth token
		And ValidateAccount body with account number "GB09HAOE91311808002318"
	When request is posted to api
	Then api returns OK http code
		And ValidateAccount api returns invalid verification result
		And ValidateAccount api returns 1 risk check message item

Scenario: Empty JSON in body
	Given a ValidateAccount request with valid Auth token
	When request is posted to api
	Then api returns Bad Request http code
		And ValidateAccount api returns "The body of the request is missing, or its format is invalid." in error message

Scenario: Empty object in body
	Given a ValidateAccount request with valid Auth token
		And ValidateAccount body is empty object
	When request is posted to api
	Then api returns Bad Request http code
		And ValidateAccount api returns "Value is required." in error message

# There should be 2 scenarios for testing actionCode values and type values
# (they are documented as enum in the task), but I don't know the behavior
# for it.
# I'd also like to implement authorization checks, but it should be
# another feature file
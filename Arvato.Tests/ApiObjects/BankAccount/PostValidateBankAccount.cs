﻿using RestSharp;

namespace Arvato.Tests.ApiObjects.BankAccount
{
    public class PostValidateBankAccount : ApiRequestBase
    {
        /*
         * For a flexibility of using various versions of API endpoints
         * It's simple implementation, might be more complex for
         * real world application (e.g. using DI)
         */
        public PostValidateBankAccount(int version = 3)
        {
            Url = $"v{version}/validate/bank-account";
            Method = Method.POST;
        }
    }
}
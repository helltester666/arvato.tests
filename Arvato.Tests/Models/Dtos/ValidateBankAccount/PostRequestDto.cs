﻿namespace Arvato.Tests.Models.Dtos.ValidateBankAccount
{
    public class PostRequestDto
    {
        public string BankAccount { get; set; }
    }
}

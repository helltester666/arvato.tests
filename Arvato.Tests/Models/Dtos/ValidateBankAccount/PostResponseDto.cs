﻿using System.Collections.Generic;

namespace Arvato.Tests.Models.Dtos.ValidateBankAccount
{
    public class PostResponseDto
    {
        public bool IsValid { get; set; }
        public List<RiskCheckMessage> RiskCheckMessages { get; set; }
    }
}

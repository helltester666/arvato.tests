﻿using Arvato.Tests.Extensions;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Linq;

namespace Arvato.Tests.ApiObjects
{
    public class ApiRequestBase
    {
        /*
         * Using Uri helps to avoid bugs like slash positioning,
         * concatenating parts of url etc. This implementation does not
         * assume testing several endpoints
         */
        public string Url;
        public Method Method;
        public object Body;

        /*
         * Simple container for request headers, it's enough for this task
         */
        public Dictionary<string, string> Headers = new Dictionary<string, string>();

        /*
         * It will be useful for negative tests
         */
        public ApiRequestBase WithValidAuthKey()
        {
            return WithHeader("X-Auth-Key", Framework.Config.AuthKey);
        }

        public ApiRequestBase WithInvalidAuthKey()
        {
            return WithHeader("X-Auth-Key", string.Empty);
        }

        /*
         * It will be useful for negative tests
         */
        public ApiRequestBase WithValidContentType()
        {
            return WithHeader("Content-Type", "application/json");
        }

        public ApiRequestBase WithHeader(string key, string value)
        {
            Headers.Add(key, value);
            return this;
        }

        public ApiRequestBase WithBody(object body)
        {
            Body = body;
            return this;
        }

        public IRestResponse Execute()
        {
            Framework.Logger.Info(ToString());
            var response = new ApiClient().Execute(this);

            Framework.Logger.Info(response.ResponseLog());
            return response;
        }

        public override string ToString()
        {
            var endpoint = $"{Method} {Framework.Config.BaseUrl}/{Url}";
            var headers = "Headers:\n" + string.Join('\n', Headers.Select(h => $"{h.Key}: {h.Value}"));
            var body = "Body:\n" + JsonConvert.SerializeObject(Body, Formatting.Indented);

            return $"====== Request ======>\n{endpoint}\n\n{headers}\n\n{body}\n";
        }
    }
}

﻿using Arvato.Tests.Models.Dtos.ValidateBankAccount.Enums;

namespace Arvato.Tests.Models.Dtos.ValidateBankAccount
{
    public class RiskCheckMessage
    {
        public Type Type { get; set; }
        public string Code { get; set; }
        public string Message { get; set; }
        public string CustomerFacingMessage { get; set; }
        public ActionCode ActionCode { get; set; }
        public string FieldReference { get; set; }
    }
}

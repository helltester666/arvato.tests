﻿using System.Linq;
using TechTalk.SpecFlow;

namespace Arvato.Tests.Steps.Transformations
{
    [Binding]
    public class ArgumentTransformations
    {
        [StepArgumentTransformation]
        public bool TransformPrepositionToBool(string preposition)
        {
            var positive = "with,contains,valid";
            var negative = "without,not contains,invalid";

            if (positive.Split(',').Contains(preposition.ToLower()))
                return true;

            if (negative.Split(',').Contains(preposition.ToLower()))
                return false;

            return false;
        }
    }
}

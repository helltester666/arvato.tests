﻿namespace Arvato.Tests.Models.Dtos.ValidateBankAccount.Enums
{
    public enum ActionCode
    {
        Unavailable,
        AskConsumerToConfirm,
        AskConsumerToReEnterData,
        OfferSecurePaymentMethods,
        RequiresSsn
    }
}

## How to run

1. Restore NuGet packages
2. Run tests via Visual Studio, Rider, VSCode or by dotnet test console command

---

## Stack

1. .Net Core 3.0
2. SpecFlow
3. RestSharp

## Idea

I use SpecFlow as a BDD framework
I have implemented custom logging because SpecFlow Reports are not available in .Net Core. So my logs are sorted inside folders with pretty clear structure:
TestResults/<TestGroup>/<ScenarioName>/log.txt. All you need to include new tests is create folder structure in project.
My approach is to use sommething like PageObjects pattern. I call it ApiObjects. Each Api object contains all information to make a request (endpoint, method, headers). I did not implement query params because of it would be excessively for this task

## How to work with steps

1. I use embedded SpecFlow DI container to share data between steps
2. I use Shared steps for common query operations (send request, set token, verify response)
3. SpecFlow hooks are separated to a file
4. SpecFlow uses argument transformations that are described in Transformations folder
5. In steps I use enums that are in Steps folder

## Scenarios

Scenarios are in the Tests folder
Tests folder has Api folder. I decided to consider a possibility of UI tests creation in the same project. So all we'll need in future is to add UI folder in Tests folder
Inside Api folder we have folder with TestGroup name. Inside this folder we have *.feature files with scenarios

## Models

Models folder contains files describes the model of requests and responses. I use the same approach as developers use for building API projects

## Helpers

Additional classes to make our work easier

## Extensions

It was needed to write an extension for IRestResonse object to have an ability to log responses
﻿using TechTalk.SpecFlow;

namespace Arvato.Tests.Steps.Shared
{
    [Binding]
    public class SpecflowHooks
    {
        [BeforeTestRun]
        public static void BeforeAll()
        {
            Framework.CreateTestResultsDirectory();
        }

        [BeforeScenario]
        public void BeforeEach()
        {
            Framework.SetLogger();
        }

        [BeforeStep]
        public void LogStep()
        {
            var stepType = ScenarioContext.Current.StepContext.StepInfo.StepDefinitionType;
            var stepText = ScenarioContext.Current.StepContext.StepInfo.Text;

            Framework.Logger.Step($"{stepType} {stepText}");
        }

        [AfterStep]
        public void LogStepResult()
        {
            var error = ScenarioContext.Current.TestError;
            if (error != null)
            {
                Framework.Logger.Error($"FAILED, exception is: {error}");
            }
            else
            {
                Framework.Logger.Info("SUCCESS");
            }
        }
    }
}

﻿using Arvato.Tests.ApiObjects;
using Arvato.Tests.Models.Dtos;
using Arvato.Tests.Models.Dtos.ValidateBankAccount;
using BoDi;
using Newtonsoft.Json;
using NUnit.Framework;
using RestSharp;
using System;
using System.Collections.Generic;
using TechTalk.SpecFlow;

namespace Arvato.Tests.Steps.ValidateAccount
{
    [Binding]
    public class ValidateAccountSteps
    {
        private readonly IObjectContainer _objectContainer;

        public ValidateAccountSteps(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        }

        [Given(@"ValidateAccount body (with|without) valid account number")]
        public void GivenValidateAccountBodyWithValidAccountNumber(bool withAccountNumber)
        {
            _objectContainer.Resolve<ApiRequestBase>().WithBody(new PostRequestDto
            {
                BankAccount = withAccountNumber ? Framework.Config.CorrectBankAccount : string.Empty
            });
        }

        [Given(@"ValidateAccount body with account number ""(.*)""")]
        public void GivenValidateAccountBodyWithAccountNumber(string number)
        {
            _objectContainer.Resolve<ApiRequestBase>().WithBody(new PostRequestDto
            {
                BankAccount = number
            });
        }

        [Given(@"ValidateAccount body is empty object")]
        public void GivenValidateAccountBodyIsEmptyObject()
        {
            _objectContainer.Resolve<ApiRequestBase>().WithBody(new Object());
        }

        [Then(@"ValidateAccount api returns (valid|invalid) verification result")]
        public void ThenValidateAccountApiReturnsOk(bool isValid)
        {
            var response = _objectContainer.Resolve<IRestResponse>();
            var responseObject = JsonConvert.DeserializeObject<PostResponseDto>(response.Content);

            if (isValid)
            {
                Assert.IsNull(responseObject.RiskCheckMessages, "RiskCheckMessages presents in response");
            }

            Assert.AreEqual(isValid, responseObject.IsValid, $"Expected isValid = {isValid}, but was {responseObject.IsValid}");
        }

        [Then(@"ValidateAccount api returns ""(.*)"" message")]
        public void ThenValidateAccountApiReturnsMessage(string message)
        {
            var response = _objectContainer.Resolve<IRestResponse>();
            var responseObject = JsonConvert.DeserializeObject<UnauthorizedResponseDto>(response.Content);

            Assert.AreEqual(message, responseObject.Message
                , $"Expected ${message} error, but was {responseObject.Message}");
        }

        [Then(@"ValidateAccount api returns ""(.*)"" in error message")]
        public void ThenValidateAccountApiReturnsInErrorMessage(string message)
        {
            var response = _objectContainer.Resolve<IRestResponse>();
            var responseObject = JsonConvert.DeserializeObject<List<RiskCheckMessage>>(response.Content);

            Assert.AreEqual(1, responseObject.Count
                , $"Expected 1 error, but was {responseObject.Count}");
            Assert.AreEqual(message, responseObject[0].Message
                , $"Expected ${message} error, but was {responseObject[0].Message}");
        }

        [Then(@"ValidateAccount api returns (.*) risk check message item")]
        public void ThenValidateAccountApiReturnsRiskCheckMessageItem(int num)
        {
            var response = _objectContainer.Resolve<IRestResponse>();
            var responseObject = JsonConvert.DeserializeObject<PostResponseDto>(response.Content);

            Assert.AreEqual(num, responseObject.RiskCheckMessages.Count
            , $"Expected {num} risk check messages, but was {responseObject.RiskCheckMessages.Count}");
        }

    }
}

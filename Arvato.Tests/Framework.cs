﻿using Arvato.Tests.Helpers;
using Newtonsoft.Json;
using NUnit.Framework;
using System.IO;
using TechTalk.SpecFlow;

namespace Arvato.Tests
{
    public class Framework
    {
        private static AppConfig _configuration;
        private static Logger _logger;

        public static DirectoryInfo CurrentTestDirectory;

        public static AppConfig Config
        {
            get
            {
                if (_configuration == null)
                {
                    var json = File.ReadAllText("settings.json");
                    _configuration = JsonConvert.DeserializeObject<AppConfig>(json);
                }

                return _configuration;
            }
        }

        public static Logger Logger
        {
            get
            {
                if (_logger == null)
                    SetLogger();
                return _logger;
            }
        }

        public static void SetLogger()
        {
            var testResultsDir = "TestResults";
            var testName = ScenarioContext.Current.ScenarioInfo.Title;
            var featureName = FeatureContext.Current.FeatureInfo.Title;
            var fullPath = $"{testResultsDir}/{featureName}/{testName}";

            CurrentTestDirectory = Directory.Exists(fullPath) ? Directory.CreateDirectory(fullPath + TestContext.CurrentContext.Test.ID) : Directory.CreateDirectory(fullPath);

            _logger = new Logger(testName, $"{CurrentTestDirectory.FullName}/log.txt");
        }

        public static DirectoryInfo CreateTestResultsDirectory()
        {
            var testDirectory = "TestResults";

            if (Directory.Exists(testDirectory))
            {
                Directory.Delete(testDirectory, true);
            }

            return Directory.CreateDirectory(testDirectory);
        }
    }
}

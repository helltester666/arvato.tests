﻿using Newtonsoft.Json;
using RestSharp;
using System.Linq;

namespace Arvato.Tests.Extensions
{
    public static class RestResponseExtension
    {
        public static string ResponseLog(this IRestResponse response)
        {
            var statusCode = $"{(int)response.StatusCode} ({response.StatusDescription})";
            var headers = "Headers:\n" + string.Join('\n', response.Headers.Select(h => $"{h.Name}: {h.Value}"));

            /*
             * Dirty unoptimized hack. Just do it because of time limits
             */
            var responseObject = JsonConvert.DeserializeObject<object>(response.Content);
            var body = "Body:\n" + JsonConvert.SerializeObject(responseObject, Formatting.Indented);

            return $"<====== Response ======\n{statusCode}\n\n{headers}\n\n{body}\n";
        }
    }
}

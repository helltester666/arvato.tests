﻿using Arvato.Tests.ApiObjects;
using RestSharp;
using System;
using System.Collections.Generic;

namespace Arvato.Tests
{
    public class ApiClient
    {
        private readonly RestClient _client;
        private readonly RestRequest _request;

        public ApiClient()
        {
            _client = new RestClient(new Uri(Framework.Config.BaseUrl));
            _request ??= new RestRequest();
        }

        private void SetEndpoint(string uri)
        {
            _request.Resource = uri;
        }

        private void SetMethod(Method method)
        {
            _request.Method = method;
        }

        /*
         * This will force client to set "application/json" for Content-Type
         */
        private void SetBody(object body)
        {
            _request.AddJsonBody(body);
        }

        private void SetHeaders(Dictionary<string, string> headers)
        {
            foreach (var header in headers)
            {
                _request.AddHeader(header.Key, header.Value);
            }
        }

        public IRestResponse Execute(ApiRequestBase request)
        {
            SetEndpoint(request.Url);
            SetMethod(request.Method);
            SetHeaders(request.Headers);
            SetBody(request.Body);

            return _client.Execute(_request);
        }
    }
}

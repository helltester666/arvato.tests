﻿namespace Arvato.Tests.Models.Dtos.ValidateBankAccount.Enums
{
    public enum Type
    {
        BusinessError,
        TechnicalError,
        NotificationMessage
    }
}
